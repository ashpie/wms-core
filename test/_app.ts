import Application from "../src/Application";
import {setupMailServer, teardownMailServer} from "./_mail_server";
import TestApp from "../src/TestApp";


export default function useApp(appSupplier?: (addr: string, port: number) => TestApp): void {
    let app: Application;

    beforeAll(async (done) => {
        await setupMailServer();
        app = appSupplier ? appSupplier('127.0.0.1', 8966) : new TestApp('127.0.0.1', 8966);

        await app.start();
        done();
    });

    afterAll(async (done) => {
        const errors = [];

        try {
            await app.stop();
        } catch (e) {
            errors.push(e);
        }

        try {
            await teardownMailServer();
        } catch (e) {
            errors.push(e);
        }

        if (errors.length > 0) throw errors;
        done();
    });
}
