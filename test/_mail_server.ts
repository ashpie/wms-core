import MailDev from "maildev";

export const MAIL_SERVER = new MailDev({
    ip: 'localhost',
});

export async function setupMailServer(): Promise<void> {
    await new Promise((resolve, reject) => MAIL_SERVER.listen((err?: Error) => {
        if (err) reject(err);
        else resolve();
    }));
}

export async function teardownMailServer(): Promise<void> {
    await new Promise((resolve, reject) => MAIL_SERVER.close((err?: Error) => {
        if (err) reject(err);
        else resolve();
    }));
}
