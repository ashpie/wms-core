import ModelComponent from "./ModelComponent";
import Model, {ModelType} from "./Model";
import ModelQuery, {ModelQueryResult, QueryFields} from "./ModelQuery";
import {Request} from "express";

export default class ModelFactory<M extends Model> {
    private static readonly factories: { [modelType: string]: ModelFactory<Model> | undefined } = {};

    public static register<M extends Model>(modelType: ModelType<M>): void {
        if (this.factories[modelType.name]) throw new Error(`Factory for type ${modelType.name} already defined.`);
        this.factories[modelType.name] = new ModelFactory<M>(modelType) as unknown as ModelFactory<Model>;
    }

    public static get<M extends Model>(modelType: ModelType<M>): ModelFactory<M> {
        const factory = this.factories[modelType.name];
        if (!factory) throw new Error(`No factory registered for ${modelType.name}.`);
        return factory as unknown as ModelFactory<M>;
    }

    public static has<M extends Model>(modelType: ModelType<M>): boolean {
        return !!this.factories[modelType.name];
    }

    private readonly modelType: ModelType<M>;
    private readonly components: ModelComponentFactory<M>[] = [];

    protected constructor(modelType: ModelType<M>) {
        this.modelType = modelType;
    }

    public addComponent(modelComponentFactory: ModelComponentFactory<M>): void {
        this.components.push(modelComponentFactory);
    }

    public hasComponent(modelComponentFactory: ModelComponentFactory<M>): boolean {
        return !!this.components.find(c => c === modelComponentFactory);
    }

    public create(data: Pick<M, keyof M>, isNewModel: boolean): M {
        const model = new this.modelType(this as unknown as ModelFactory<never>, isNewModel);
        for (const component of this.components) {
            model.addComponent(new component(model));
        }
        model.updateWithData(data);
        return model;
    }

    public get table(): string {
        return this.modelType.table;
    }

    public select(...fields: QueryFields): ModelQuery<M> {
        return ModelQuery.select(this, ...fields);
    }

    public insert(data: Pick<M, keyof M>): ModelQuery<M> {
        return ModelQuery.insert(this, data);
    }

    public update(data: Pick<M, keyof M>): ModelQuery<M> {
        return ModelQuery.update(this, data);
    }

    public delete(): ModelQuery<M> {
        return ModelQuery.delete(this);
    }

    public getPrimaryKeyFields(): (keyof M & string)[] {
        return this.modelType.getPrimaryKeyFields();
    }

    public getPrimaryKey(modelData: Pick<M, keyof M>): Pick<M, keyof M>[keyof M & string][] {
        return this.getPrimaryKeyFields().map(f => modelData[f]);
    }

    public getPrimaryKeyString(modelData: Pick<M, keyof M>): string {
        return this.getPrimaryKey(modelData).join(',');
    }

    public async getById(...id: PrimaryKeyValue[]): Promise<M | null> {
        let query = this.select();
        const primaryKeyFields = this.getPrimaryKeyFields();
        for (let i = 0; i < primaryKeyFields.length; i++) {
            query = query.where(primaryKeyFields[i], id[i]);
        }
        return await query.first();
    }

    public async paginate(request: Request, perPage: number = 20, query?: ModelQuery<M>): Promise<ModelQueryResult<M>> {
        const page = request.params.page ? parseInt(request.params.page) : 1;
        if (!query) query = this.select();
        if (request.params.sortBy) {
            const dir = request.params.sortDirection;
            query = query.sortBy(request.params.sortBy, dir === 'ASC' || dir === 'DESC' ? dir : undefined);
        } else {
            query = query.sortBy('id');
        }
        return await query.paginate(page, perPage);
    }
}

export type ModelComponentFactory<M extends Model> = new (model: M) => ModelComponent<M>;

export type PrimaryKeyValue = string | number | boolean | null | undefined;
