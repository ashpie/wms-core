import ModelQuery, {ModelFieldData, ModelQueryResult, WhereTest} from "./ModelQuery";
import Model, {ModelType} from "./Model";
import ModelFactory from "./ModelFactory";

export default abstract class ModelRelation<S extends Model, O extends Model, R extends O | O[] | null> {
    protected readonly model: S;
    protected readonly foreignModelType: ModelType<O>;
    protected readonly dbProperties: RelationDatabaseProperties;
    protected readonly queryModifiers: QueryModifier<O>[] = [];
    protected readonly filters: ModelFilter<O>[] = [];
    protected cachedModels?: O[];

    protected constructor(model: S, foreignModelType: ModelType<O>, dbProperties: RelationDatabaseProperties) {
        this.model = model;
        this.foreignModelType = foreignModelType;
        this.dbProperties = dbProperties;
    }

    public abstract clone(): ModelRelation<S, O, R>;

    public constraint(queryModifier: QueryModifier<O>): this {
        this.queryModifiers.push(queryModifier);
        return this;
    }

    public filter(modelFilter: ModelFilter<O>): this {
        this.filters.push(modelFilter);
        return this;
    }

    protected makeQuery(): ModelQuery<O> {
        const query = ModelFactory.get(this.foreignModelType).select();
        for (const modifier of this.queryModifiers) modifier(query);
        return query;
    }

    public getModelId(): ModelFieldData {
        return this.model[this.dbProperties.localKey];
    }

    protected applyRegularConstraints(query: ModelQuery<O>): void {
        query.where(this.dbProperties.foreignKey, this.getModelId());
    }

    public async get(): Promise<R> {
        if (this.cachedModels === undefined) {
            const query = this.makeQuery();
            this.applyRegularConstraints(query);
            this.cachedModels = await query.get();
        }

        let models = this.cachedModels;
        for (const filter of this.filters) {
            const newModels = [];
            for (const model of models) {
                if (await filter(model)) {
                    newModels.push(model);
                }
            }
            models = newModels;
        }
        return this.collectionToOutput(models);
    }

    public getOrFail(): R {
        if (this.cachedModels === undefined) throw new Error('Models were not fetched');
        return this.collectionToOutput(this.cachedModels);
    }

    protected abstract collectionToOutput(models: O[]): R;

    public async eagerLoad(
        relations: ModelRelation<S, O, R>[],
        subRelations: string[] = [],
    ): Promise<ModelQueryResult<O>> {
        const ids = relations.map(r => r.getModelId())
            .filter(id => id !== null && id !== undefined)
            .reduce((array: ModelFieldData[], val) => array.indexOf(val) >= 0 ? array : [...array, val], []);
        if (ids.length === 0) return [];

        const query = this.makeQuery();
        query.where(this.dbProperties.foreignKey, ids, WhereTest.IN);
        query.with(...subRelations);
        return await query.get();
    }

    public async populate(models: ModelQueryResult<O>): Promise<void> {
        this.cachedModels = models.filter(m => m[this.dbProperties.foreignKey] === this.getModelId())
            .reduce((array: O[], val) => array.find(v => v.equals(val)) ? array : [...array, val], []);
    }

    public async count(): Promise<number> {
        const models = await this.get();
        if (Array.isArray(models)) return models.length;
        else return models !== null ? 1 : 0;
    }

    public async has(model: O): Promise<boolean> {
        const models = await this.get();
        if (Array.isArray(models)) return models.find(m => m.equals(model)) !== undefined;
        else return models && models.equals(model);
    }
}

export class OneModelRelation<S extends Model, O extends Model> extends ModelRelation<S, O, O | null> {
    public constructor(model: S, foreignModelType: ModelType<O>, dbProperties: RelationDatabaseProperties) {
        super(model, foreignModelType, dbProperties);
    }

    public clone(): OneModelRelation<S, O> {
        return new OneModelRelation(this.model, this.foreignModelType, this.dbProperties);
    }

    protected collectionToOutput(models: O[]): O | null {
        return models[0] || null;
    }

    public async set(model: O): Promise<void> {
        (this.model as Model)[this.dbProperties.localKey] = model[this.dbProperties.foreignKey];
    }

    public async clear(): Promise<void> {
        (this.model as Model)[this.dbProperties.localKey] = undefined;
    }
}

export class ManyModelRelation<S extends Model, O extends Model> extends ModelRelation<S, O, O[]> {
    protected readonly paginatedCache: {
        [perPage: number]: {
            [pageNumber: number]: ModelQueryResult<O> | undefined
        } | undefined
    } = {};

    public constructor(model: S, foreignModelType: ModelType<O>, dbProperties: RelationDatabaseProperties) {
        super(model, foreignModelType, dbProperties);
    }

    public clone(): ManyModelRelation<S, O> {
        return new ManyModelRelation<S, O>(this.model, this.foreignModelType, this.dbProperties);
    }

    public cloneReduceToOne(): OneModelRelation<S, O> {
        return new OneModelRelation<S, O>(this.model, this.foreignModelType, this.dbProperties);
    }

    protected collectionToOutput(models: O[]): O[] {
        return models;
    }

    public async paginate(page: number, perPage: number): Promise<ModelQueryResult<O>> {
        let cache = this.paginatedCache[perPage];
        if (!cache) cache = this.paginatedCache[perPage] = {};

        let cachePage = cache[page];
        if (!cachePage) {
            const query = this.makeQuery();
            this.applyRegularConstraints(query);
            cachePage = cache[page] = await query.paginate(page, perPage);
        }

        return cachePage;
    }
}

export class ManyThroughModelRelation<S extends Model, O extends Model> extends ManyModelRelation<S, O> {
    protected readonly dbProperties: PivotRelationDatabaseProperties;

    public constructor(model: S, foreignModelType: ModelType<O>, dbProperties: PivotRelationDatabaseProperties) {
        super(model, foreignModelType, dbProperties);
        this.dbProperties = dbProperties;
        this.constraint(query => query
            .leftJoin(this.dbProperties.pivotTable, 'pivot')
            .on(`pivot.${this.dbProperties.foreignPivotKey}`, `${this.foreignModelType.table}.${this.dbProperties.foreignKey}`),
        );
    }

    public clone(): ManyThroughModelRelation<S, O> {
        return new ManyThroughModelRelation<S, O>(this.model, this.foreignModelType, this.dbProperties);
    }

    public cloneReduceToOne(): OneModelRelation<S, O> {
        throw new Error('Cannot reduce many through relation to one model.');
    }

    protected applyRegularConstraints(query: ModelQuery<O>): void {
        query.where(`pivot.${this.dbProperties.localPivotKey}`, this.getModelId());
    }

    public async eagerLoad(
        relations: ModelRelation<S, O, O[]>[],
        subRelations: string[] = [],
    ): Promise<ModelQueryResult<O>> {
        const ids = relations.map(r => r.getModelId())
            .reduce((array: ModelFieldData[], val) => array.indexOf(val) >= 0 ? array : [...array, val], []);
        if (ids.length === 0) return [];

        const query = this.makeQuery();
        query.where(`pivot.${this.dbProperties.localPivotKey}`, ids, WhereTest.IN);
        query.pivot(`pivot.${this.dbProperties.localPivotKey}`, `pivot.${this.dbProperties.foreignPivotKey}`);
        query.with(...subRelations);
        return await query.get();
    }

    public async populate(models: ModelQueryResult<O>): Promise<void> {
        if (!models.pivot) throw new Error('ModelQueryResult.pivot not loaded.');
        const ids = models.pivot
            .filter(p => p[`pivot.${this.dbProperties.localPivotKey}`] === this.getModelId())
            .map(p => p[`pivot.${this.dbProperties.foreignPivotKey}`]);
        this.cachedModels = models.filter(m => ids.indexOf(m[this.dbProperties.foreignKey]) >= 0)
            .reduce((array: O[], val) => array.find(v => v.equals(val)) ? array : [...array, val], []);
    }
}

export class RecursiveModelRelation<M extends Model> extends ManyModelRelation<M, M> {
    private readonly reverse: boolean;

    public constructor(
        model: M,
        foreignModelType: ModelType<M>,
        dbProperties: RelationDatabaseProperties,
        reverse: boolean = false,
    ) {
        super(model, foreignModelType, dbProperties);
        this.constraint(query => query.recursive(this.dbProperties, reverse));
        this.reverse = reverse;
    }

    public clone(): RecursiveModelRelation<M> {
        return new RecursiveModelRelation(this.model, this.foreignModelType, this.dbProperties);
    }

    public async populate(models: ModelQueryResult<M>): Promise<void> {
        await super.populate(models);
        const cachedModels = this.cachedModels;
        if (cachedModels) {
            let count;
            do {
                count = cachedModels.length;
                cachedModels.push(...models.filter(model =>
                    !cachedModels.find(cached => cached.equals(model)) && cachedModels.find(cached => {
                        return cached[this.dbProperties.localKey] === model[this.dbProperties.foreignKey];
                    }),
                ).reduce((array: M[], val) => array.find(v => v.equals(val)) ? array : [...array, val], []));
            } while (count !== cachedModels.length);

            if (this.reverse) cachedModels.reverse();
        }
    }

}

export type QueryModifier<M extends Model> = (query: ModelQuery<M>) => ModelQuery<M>;

export type ModelFilter<O extends Model> = (model: O) => boolean | Promise<boolean>;

export type RelationDatabaseProperties = {
    localKey: string;
    foreignKey: string;
};

export type PivotRelationDatabaseProperties = RelationDatabaseProperties & {
    pivotTable: string;
    localPivotKey: string;
    foreignPivotKey: string;
};
