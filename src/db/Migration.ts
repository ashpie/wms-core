import {Connection} from "mysql";
import MysqlConnectionManager from "./MysqlConnectionManager";
import {Type} from "../Utils";

export default abstract class Migration {
    public readonly version: number;
    private currentConnection?: Connection;

    public constructor(version: number) {
        this.version = version;
    }

    public async shouldRun(currentVersion: number): Promise<boolean> {
        return this.version > currentVersion;
    }

    public abstract async install(): Promise<void>;

    public abstract async rollback(): Promise<void>;

    public registerModels?(): void;

    protected async query(queryString: string): Promise<void> {
        await MysqlConnectionManager.query(queryString, undefined, this.getCurrentConnection());
    }

    protected getCurrentConnection(): Connection {
        if (!this.currentConnection) throw new Error('No current connection set.');
        return this.currentConnection;
    }

    public setCurrentConnection(connection: Connection | null): void {
        this.currentConnection = connection || undefined;
    }
}

export interface MigrationType<M extends Migration> extends Type<M> {
    new(version: number): M;
}
