import {Express, Router} from "express";
import {log} from "./Logger";
import {sleep} from "./Utils";
import Application from "./Application";
import config from "config";
import SecurityError from "./SecurityError";
import Middleware, {MiddlewareType} from "./Middleware";

export default abstract class ApplicationComponent {
    private currentRouter?: Router;
    private app?: Application;

    public async checkSecuritySettings?(): Promise<void>;

    public async start?(expressApp: Express): Promise<void>;

    public async init?(router: Router): Promise<void>;

    public async handle?(router: Router): Promise<void>;

    public async stop?(): Promise<void>;

    protected async prepare(name: string, prepare: () => Promise<void>): Promise<void> {
        let err;
        do {
            try {
                await prepare();
                err = null;
            } catch (e) {
                err = e;
                log.error(err, `${name} failed to prepare; retrying in 5s...`);
                await sleep(5000);
            }
        } while (err);
        log.info(`${name} ready!`);
    }

    protected async close(thingName: string, fn: (callback: (err?: Error | null) => void) => void): Promise<void> {
        try {
            await new Promise((resolve, reject) => fn((err?: Error | null) => {
                if (err) reject(err);
                else resolve();
            }));

            log.info(`${thingName} closed.`);
        } catch (e) {
            log.error(e, `An error occurred while closing the ${thingName}.`);
        }
    }

    protected checkSecurityConfigField(field: string): void {
        if (!config.has(field) || config.get<string>(field) === 'default') {
            throw new SecurityError(`${field} field not configured.`);
        }
    }

    protected use<M extends Middleware>(middleware: MiddlewareType<M>): void {
        if (!this.currentRouter) throw new Error('Cannot call this method outside init() and handle().');

        const instance = new middleware(this.getApp());
        this.currentRouter.use(async (req, res, next) => {
            try {
                await instance.getRequestHandler()(req, res, next);
            } catch (e) {
                next(e);
            }
        });
    }

    public setCurrentRouter(router: Router | null): void {
        this.currentRouter = router || undefined;
    }

    protected getApp(): Application {
        if (!this.app) throw new Error('app field not initialized.');
        return this.app;
    }

    public setApp(app: Application): void {
        this.app = app;
    }
}
