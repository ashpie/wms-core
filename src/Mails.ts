import config from "config";
import {MailTemplate} from "./Mail";

export const MAGIC_LINK_MAIL = new MailTemplate(
    'magic_link',
    data => data.type === 'register' ? 'Registration' : 'Login magic link',
);

export const ACCOUNT_REVIEW_NOTICE_MAIL_TEMPLATE: MailTemplate = new MailTemplate(
    'account_review_notice',
    data => `Your account was ${data.approved ? 'approved' : 'rejected'}.`,
);

export const PENDING_ACCOUNT_REVIEW_MAIL_TEMPLATE: MailTemplate = new MailTemplate(
    'pending_account_review',
    () => 'A new account is pending review on ' + config.get<string>('app.name'),
);
