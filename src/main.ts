import {delimiter} from "path";

// Load config from specified path or default + wms-core/config (default defaults)
process.env['NODE_CONFIG_DIR'] =
    __dirname + '/../../node_modules/wms-core/config/'
    + delimiter
    + (process.env['NODE_CONFIG_DIR'] || __dirname + '/../../config/');

import {log} from "./Logger";
import TestApp from "./TestApp";
import config from "config";

(async () => {
    log.debug('Config path:', process.env['NODE_CONFIG_DIR']);

    const app = new TestApp(config.get<string>('listen_addr'), config.get<number>('port'));
    await app.start();
})().catch(err => {
    log.error(err);
});
