export default interface CacheProvider {
    get<T extends string | undefined>(key: string, defaultValue?: T): Promise<T>;

    has(key: string): Promise<boolean>;

    forget(key: string): Promise<void>;

    /**
     * @param key
     * @param value
     * @param ttl in ms
     */
    remember(key: string, value: string, ttl: number): Promise<void>;
}
