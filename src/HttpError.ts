import {WrappingError} from "./Utils";

export abstract class HttpError extends WrappingError {
    public readonly instructions: string;

    protected constructor(message: string, instructions: string, cause?: Error) {
        super(message, cause);
        this.instructions = instructions;
    }

    public get name(): string {
        return this.constructor.name;
    }

    abstract get errorCode(): number;
}

export class BadRequestError extends HttpError {
    public readonly url: string;

    public constructor(message: string, instructions: string, url: string, cause?: Error) {
        super(message, instructions, cause);
        this.url = url;
    }

    public get errorCode(): number {
        return 400;
    }
}

export class UnauthorizedHttpError extends BadRequestError {
    public constructor(message: string, url: string, cause?: Error) {
        super(message, '', url, cause);
    }

    public get errorCode(): number {
        return 401;
    }
}

export class ForbiddenHttpError extends BadRequestError {
    public constructor(thing: string, url: string, cause?: Error) {
        super(
            `You don't have access to this ${thing}.`,
            `${url} doesn't belong to *you*.`,
            url,
            cause,
        );
    }

    public get errorCode(): number {
        return 403;
    }
}

export class NotFoundHttpError extends BadRequestError {
    public constructor(thing: string, url: string, cause?: Error) {
        super(
            `${thing.charAt(0).toUpperCase()}${thing.substr(1)} not found.`,
            `${url} doesn't exist or was deleted.`,
            url,
            cause,
        );
    }

    public get errorCode(): number {
        return 404;
    }
}

export class TooManyRequestsHttpError extends BadRequestError {
    public constructor(retryIn: number, cause?: Error) {
        super(
            `You're making too many requests!`,
            `We need some rest. Please retry in ${Math.floor(retryIn / 1000)} seconds.`,
            '',
            cause,
        );
    }

    public get errorCode(): number {
        return 429;
    }
}

export class ServerError extends HttpError {
    public constructor(message: string, cause?: Error) {
        super(message, `Maybe you should contact us; see instructions below.`, cause);
    }

    public get errorCode(): number {
        return 500;
    }
}

export class ServiceUnavailableHttpError extends ServerError {
    public get errorCode(): number {
        return 503;
    }
}
