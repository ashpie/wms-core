import ModelComponent from "../db/ModelComponent";
import User from "./models/User";

export const USERNAME_REGEXP = /^[0-9a-z_-]+$/;

export default class UserNameComponent extends ModelComponent<User> {
    public name?: string = undefined;

    public init(): void {
        this.setValidation('name').defined().between(3, 64).regexp(USERNAME_REGEXP).unique(this._model);
    }
}
