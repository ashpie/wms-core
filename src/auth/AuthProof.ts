/**
 * This class is most commonly used for authentication. It can be more generically used to represent a verification
 * state of whether a given resource is owned by a session.
 *
 * Any auth system should consider this auth proof valid if and only if both {@code isValid()} and
 * {@code isAuthorized()} both return {@code true}.
 *
 * @type <R> The resource type this AuthProof authorizes.
 */
export default interface AuthProof<R> {
    /**
     * Is this auth proof valid in time (and context)?
     *
     * For example, it can return true for an initial short validity time period then false, and increase that time
     * period if {@code isAuthorized()} returns true.
     */
    isValid(): Promise<boolean>;

    /**
     * Was this proof authorized?
     *
     * Return true once the session is proven to own the associated resource.
     */
    isAuthorized(): Promise<boolean>;

    /**
     * Retrieve the resource this auth proof is supposed to authorize.
     * If this resource doesn't exist yet, return {@code null}.
     */
    getResource(): Promise<R | null>;

    /**
     * Manually revokes this authentication proof. Once this method is called, all of the following must be true:
     * - {@code isAuthorized} returns {@code false}
     * - There is no way to re-authorize this proof (i.e. {@code isAuthorized} can never return {@code true} again)
     *
     * Additionally, this method should delete any stored data that could lead to restoration of this AuthProof
     * instance.
     */
    revoke(): Promise<void>;
}
