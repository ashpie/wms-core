import ModelComponent from "../../db/ModelComponent";
import User from "./User";

export default class UserApprovedComponent extends ModelComponent<User> {
    public approved: boolean = false;
}
