import User from "./User";
import Model from "../../db/Model";
import {OneModelRelation} from "../../db/ModelRelation";
import {EMAIL_REGEX} from "../../db/Validator";

export default class UserEmail extends Model {
    public readonly id?: number = undefined;
    public user_id?: number = undefined;
    public readonly email?: string = undefined;
    public created_at?: Date = undefined;

    public readonly user = new OneModelRelation(this, User, {
        localKey: 'user_id',
        foreignKey: 'id',
    });

    protected init(): void {
        this.setValidation('user_id').acceptUndefined().exists(User, 'id');
        this.setValidation('email').defined().regexp(EMAIL_REGEX).unique(this);
        this.setValidation('main').defined();
    }
}
