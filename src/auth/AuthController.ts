import Controller from "../Controller";
import {NextFunction, Request, Response} from "express";
import AuthComponent, {AuthMiddleware, RequireAuthMiddleware, RequireGuestMiddleware} from "./AuthComponent";
import {BadRequestError} from "../HttpError";

export default class AuthController extends Controller {
    public getRoutesPrefix(): string {
        return '/auth';
    }

    public routes(): void {
        this.use(async (req, res, next) => {
            const authGuard = this.getApp().as(AuthComponent).getAuthGuard();
            if (await authGuard.interruptAuth(req, res)) return;
            next();
        });

        this.get('/', this.getAuth, 'auth', RequireGuestMiddleware);
        this.post('/login', this.postLogin, 'login', RequireGuestMiddleware);
        this.post('/register', this.postRegister, 'register', RequireGuestMiddleware);
        this.post('/logout', this.postLogout, 'logout', RequireAuthMiddleware);
    }

    protected async getAuth(req: Request, res: Response, _next: NextFunction): Promise<void> {
        const authGuard = this.getApp().as(AuthComponent).getAuthGuard();

        res.render('auth/auth', {
            auth_methods: authGuard.getAuthMethodNames(),
        });
    }

    protected async postLogin(req: Request, res: Response): Promise<void> {
        return await this.handleAuth(req, res, false);
    }

    protected async postRegister(req: Request, res: Response): Promise<void> {
        return await this.handleAuth(req, res, true);
    }

    protected async handleAuth(req: Request, res: Response, isRegistration: boolean): Promise<void> {
        const authGuard = this.getApp().as(AuthComponent).getAuthGuard();

        const identifier = req.body.identifier;
        if (!identifier) throw new BadRequestError('Identifier not specified.', 'Please try again.', req.originalUrl);

        // Get requested auth method
        if (req.body.auth_method) {
            const method = await authGuard.getAuthMethodByName(req.body.auth_method);
            if (!method) {
                throw new BadRequestError('Invalid auth method: ' + req.body.auth_method,
                    'Available methods are: ' + authGuard.getAuthMethodNames(), req.url);
            }

            const user = await method.findUserByIdentifier(identifier);
            if (!user) { // Register
                return isRegistration ?
                    await method.attemptRegister(req, res, identifier) :
                    await this.redirectToRegistration(req, res, identifier);
            }

            // Login
            return await method.attemptLogin(req, res, user);
        }


        const methods = await authGuard.getAuthMethodsByIdentifier(identifier);

        if (methods.length === 0) { // Register
            return isRegistration ?
                await authGuard.getRegistrationMethod().attemptRegister(req, res, identifier) :
                await this.redirectToRegistration(req, res, identifier);
        }

        const {user, method} = methods[0];
        return await method.attemptLogin(req, res, user);
    }

    protected async postLogout(req: Request, res: Response, _next: NextFunction): Promise<void> {
        const userId = typeof req.body.user_id === 'string' ? parseInt(req.body.user_id) : null;

        const proofs = await req.as(AuthMiddleware).getAuthGuard().getProofs(req);

        for (const proof of proofs) {
            if (userId === null || (await proof.getResource())?.id === userId) {
                await proof.revoke();
            }
        }

        req.flash('success', 'Successfully logged out.');
        res.redirect(req.query.redirect_uri?.toString() || '/');
    }

    protected async redirectToRegistration(req: Request, res: Response, identifier: string): Promise<void> {
        req.flash('register_identifier', identifier);
        req.flash('info', `User with identifier "${identifier}" not found.`);
        res.redirect(Controller.route('auth', undefined, {
            redirect_uri: req.query.redirect_uri?.toString() || undefined,
        }));
    }
}
