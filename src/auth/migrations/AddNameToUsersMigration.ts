import Migration from "../../db/Migration";
import ModelFactory from "../../db/ModelFactory";
import User from "../models/User";
import UserNameComponent from "../UserNameComponent";

export default class AddNameToUsersMigration extends Migration {
    public async install(): Promise<void> {
        await this.query(`ALTER TABLE users
            ADD COLUMN name VARCHAR(64) UNIQUE NOT NULL`);
    }

    public async rollback(): Promise<void> {
        await this.query('ALTER TABLE users DROP COLUMN name');
    }

    public registerModels(): void {
        ModelFactory.get(User).addComponent(UserNameComponent);
    }
}
