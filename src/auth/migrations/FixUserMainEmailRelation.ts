import Migration from "../../db/Migration";

/**
 * @deprecated - TODO may be remove at next major version >= 0.24, replace with DummyMigration.
 */
export default class FixUserMainEmailRelation extends Migration {
    public async install(): Promise<void> {
        await this.query(`ALTER TABLE users
            ADD COLUMN main_email_id INT,
            ADD FOREIGN KEY main_user_email_fk (main_email_id) REFERENCES user_emails (id)`);
        await this.query(`UPDATE users u LEFT JOIN user_emails ue ON u.id = ue.user_id
                          SET u.main_email_id=ue.id
                          WHERE ue.main = true`);
        await this.query(`ALTER TABLE user_emails
            DROP COLUMN main`);
    }

    public async rollback(): Promise<void> {
        await this.query(`ALTER TABLE user_emails
            ADD COLUMN main BOOLEAN DEFAULT false`);
        await this.query(`UPDATE user_emails ue LEFT JOIN users u ON ue.id = u.main_email_id
                          SET ue.main = true`);
        await this.query(`ALTER TABLE users
            DROP FOREIGN KEY main_user_email_fk,
            DROP COLUMN main_email_id`);
    }
}
