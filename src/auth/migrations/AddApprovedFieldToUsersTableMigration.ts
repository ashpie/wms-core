import Migration from "../../db/Migration";
import ModelFactory from "../../db/ModelFactory";
import User from "../models/User";
import UserApprovedComponent from "../models/UserApprovedComponent";

export default class AddApprovedFieldToUsersTableMigration extends Migration {
    public async install(): Promise<void> {
        await this.query('ALTER TABLE users ADD COLUMN approved BOOLEAN NOT NULL DEFAULT 0');
    }

    public async rollback(): Promise<void> {
        await this.query('ALTER TABLE users DROP COLUMN approved');
    }

    public registerModels(): void {
        ModelFactory.get(User).addComponent(UserApprovedComponent);
    }
}
