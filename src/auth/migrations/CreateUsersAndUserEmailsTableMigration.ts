import Migration from "../../db/Migration";
import ModelFactory from "../../db/ModelFactory";
import User from "../models/User";
import UserEmail from "../models/UserEmail";

export default class CreateUsersAndUserEmailsTableMigration extends Migration {
    public async install(): Promise<void> {
        await this.query(`CREATE TABLE users
            (
                id         INT      NOT NULL AUTO_INCREMENT,
                main_email_id INT,
                is_admin   BOOLEAN  NOT NULL DEFAULT false,
                created_at DATETIME NOT NULL DEFAULT NOW(),
                updated_at DATETIME NOT NULL DEFAULT NOW(),
                PRIMARY KEY (id)
            )`);
        await this.query(`CREATE TABLE user_emails
            (
                id         INT                 NOT NULL AUTO_INCREMENT,
                user_id    INT                 NOT NULL,
                email      VARCHAR(254) UNIQUE NOT NULL,
                created_at DATETIME            NOT NULL DEFAULT NOW(),
                PRIMARY KEY (id),
                FOREIGN KEY user_fk (user_id) REFERENCES users (id) ON DELETE CASCADE
            )`);
        await this.query(`ALTER TABLE users
            ADD FOREIGN KEY main_user_email_fk (main_email_id) REFERENCES user_emails (id) ON DELETE SET NULL`);
    }

    public async rollback(): Promise<void> {
        await this.query('DROP TABLE user_emails');
        await this.query('DROP TABLE users');
    }

    public registerModels(): void {
        ModelFactory.register(User);
        ModelFactory.register(UserEmail);
    }
}
