import Migration from "../../db/Migration";

/**
 * @deprecated - TODO may be remove at next major version >= 0.24, replace with DummyMigration.
 */
export default class DropNameFromUsers extends Migration {
    public async install(): Promise<void> {
        await this.query('ALTER TABLE users DROP COLUMN name');
    }

    public async rollback(): Promise<void> {
        await this.query('ALTER TABLE users ADD COLUMN name VARCHAR(64)');
    }
}
