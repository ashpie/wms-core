import AuthProof from "./AuthProof";
import MysqlConnectionManager from "../db/MysqlConnectionManager";
import User from "./models/User";
import {Connection} from "mysql";
import {Request, Response} from "express";
import {PENDING_ACCOUNT_REVIEW_MAIL_TEMPLATE} from "../Mails";
import Mail from "../Mail";
import Controller from "../Controller";
import config from "config";
import Application from "../Application";
import NunjucksComponent from "../components/NunjucksComponent";
import AuthMethod from "./AuthMethod";

export default class AuthGuard {
    private readonly authMethods: AuthMethod<AuthProof<User>>[];

    public constructor(
        private readonly app: Application,
        ...authMethods: AuthMethod<AuthProof<User>>[]
    ) {
        this.authMethods = authMethods;
    }

    public async interruptAuth(req: Request, res: Response): Promise<boolean> {
        for (const method of this.authMethods) {
            if (method.interruptAuth && await method.interruptAuth(req, res)) return true;
        }

        return false;
    }

    public getAuthMethodByName(authMethodName: string): AuthMethod<AuthProof<User>> | null {
        return this.authMethods.find(m => m.getName() === authMethodName) || null;
    }

    public getAuthMethodNames(): string[] {
        return this.authMethods.map(m => m.getName());
    }

    public getRegistrationMethod(): AuthMethod<AuthProof<User>> {
        return this.authMethods[0];
    }

    public async getAuthMethodsByIdentifier(
        identifier: string,
    ): Promise<{ user: User, method: AuthMethod<AuthProof<User>> }[]> {
        const methods = [];
        for (const method of this.authMethods) {
            const user = await method.findUserByIdentifier(identifier);
            if (user) methods.push({user, method});
        }
        return methods;
    }

    public async getProofs(req: Request): Promise<AuthProof<User>[]> {
        const proofs = [];
        if (req.session) {
            proofs.push(...await this.getProofsForSession(req.session));
        }
        proofs.push(...await this.getProofsForRequest(req));
        return proofs;
    }

    public async getProofsForSession(session: Express.Session): Promise<AuthProof<User>[]> {
        if (!session.is_authenticated) return [];

        const proofs = [];
        for (const method of this.authMethods) {
            if (method.getProofsForSession) {
                const methodProofs = await method.getProofsForSession(session);
                for (const proof of methodProofs) {
                    if (!await proof.isValid() || !await proof.isAuthorized()) {
                        await proof.revoke();
                    } else {
                        proofs.push(proof);
                    }
                }
            }
        }

        if (proofs.length === 0) {
            session.is_authenticated = false;
        }

        return proofs;
    }

    public async getProofsForRequest(req: Request): Promise<AuthProof<User>[]> {
        const proofs = [];
        for (const method of this.authMethods) {
            if (method.getProofsForRequest) {
                const methodProofs = await method.getProofsForRequest(req);
                for (const proof of methodProofs) {
                    if (!await proof.isValid() || !await proof.isAuthorized()) {
                        await proof.revoke();
                    } else {
                        proofs.push(proof);
                    }
                }
            }
        }

        return proofs;
    }

    public async authenticateOrRegister(
        session: Express.Session,
        proof: AuthProof<User>,
        onLogin?: (user: User) => Promise<void>,
        beforeRegister?: (connection: Connection, user: User) => Promise<RegisterCallback[]>,
        afterRegister?: (connection: Connection, user: User) => Promise<RegisterCallback[]>,
    ): Promise<User> {
        if (!await proof.isValid()) throw new InvalidAuthProofError();
        if (!await proof.isAuthorized()) throw new UnauthorizedAuthProofError();

        let user = await proof.getResource();

        // Register if user doesn't exist
        if (!user) {
            const callbacks: RegisterCallback[] = [];

            user = await MysqlConnectionManager.wrapTransaction(async connection => {
                const user = User.create({});
                if (beforeRegister) {
                    (await beforeRegister(connection, user)).forEach(c => callbacks.push(c));
                }
                await user.save(connection, c => callbacks.push(c));
                if (afterRegister) {
                    (await afterRegister(connection, user)).forEach(c => callbacks.push(c));
                }
                return user;
            });

            for (const callback of callbacks) {
                await callback();
            }

            if (!user.isApproved()) {
                await new Mail(this.app.as(NunjucksComponent).getEnvironment(), PENDING_ACCOUNT_REVIEW_MAIL_TEMPLATE, {
                    username: (await user.mainEmail.get())?.getOrFail('email'),
                    link: config.get<string>('base_url') + Controller.route('accounts-approval'),
                }).send(config.get<string>('app.contact_email'));
            }
        }

        // Don't login if user isn't approved
        if (!user.isApproved()) {
            throw new PendingApprovalAuthError();
        }

        // Login
        session.is_authenticated = true;
        if (onLogin) await onLogin(user);

        return user;
    }
}

export class AuthError extends Error {
}

export class AuthProofError extends AuthError {
}

export class InvalidAuthProofError extends AuthProofError {
    public constructor() {
        super('Invalid auth proof.');
    }
}

export class UnauthorizedAuthProofError extends AuthProofError {
    public constructor() {
        super('Unauthorized auth proof.');
    }
}

export class PendingApprovalAuthError extends AuthError {
    public constructor() {
        super(`User is not approved.`);
    }
}

export type RegisterCallback = () => Promise<void>;
