import Migration from "../../db/Migration";
import ModelFactory from "../../db/ModelFactory";
import User from "../models/User";
import UserPasswordComponent from "./UserPasswordComponent";

export default class AddPasswordToUsersMigration extends Migration {
    public async install(): Promise<void> {
        await this.query(`ALTER TABLE users
            ADD COLUMN password VARCHAR(128) NOT NULL`);
    }

    public async rollback(): Promise<void> {
        await this.query(`ALTER TABLE users
            DROP COLUMN password`);
    }

    public registerModels(): void {
        ModelFactory.get(User).addComponent(UserPasswordComponent);
    }
}
