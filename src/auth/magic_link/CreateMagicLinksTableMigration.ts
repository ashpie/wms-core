import Migration from "../../db/Migration";
import ModelFactory from "../../db/ModelFactory";
import MagicLink from "../models/MagicLink";

export default class CreateMagicLinksTableMigration extends Migration {
    public async install(): Promise<void> {
        await this.query(`CREATE TABLE magic_links
            (
                id           INT             NOT NULL AUTO_INCREMENT,
                session_id   CHAR(32) UNIQUE NOT NULL,
                email        VARCHAR(254)    NOT NULL,
                token        CHAR(96)        NOT NULL,
                action_type  VARCHAR(64)     NOT NULL,
                original_url VARCHAR(1745)   NOT NULL,
                generated_at DATETIME        NOT NULL,
                authorized   BOOLEAN         NOT NULL,
                PRIMARY KEY (id)
            )`);
    }

    public async rollback(): Promise<void> {
        await this.query('DROP TABLE magic_links');
    }

    public registerModels(): void {
        ModelFactory.register(MagicLink);
    }

}
