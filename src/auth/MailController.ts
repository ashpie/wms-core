import {Request, Response} from "express";
import Controller from "../Controller";
import Mail from "../Mail";
import NunjucksComponent from "../components/NunjucksComponent";

export default class MailController extends Controller {
    public routes(): void {
        this.get("/mail/:template", this.getMail, 'mail');
    }

    protected async getMail(request: Request, response: Response): Promise<void> {
        const template = request.params['template'];
        response.send(Mail.parse(this.getApp().as(NunjucksComponent).getEnvironment(),
            `mails/${template}.mjml.njk`, request.query, false));
    }
}
