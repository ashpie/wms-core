import User from "./models/User";
import AuthProof from "./AuthProof";
import {Request, Response} from "express";


export default interface AuthMethod<P extends AuthProof<User>> {
    /**
     * @return A unique name.
     */
    getName(): string;

    findUserByIdentifier(identifier: string): Promise<User | null>;

    getProofsForSession?(session: Express.Session): Promise<P[]>;

    getProofsForRequest?(req: Request): Promise<P[]>;

    /**
     * @return {@code true} if interrupted, {@code false} otherwise.
     */
    interruptAuth?(req: Request, res: Response): Promise<boolean>;

    attemptLogin(req: Request, res: Response, user: User): Promise<void>;

    attemptRegister(req: Request, res: Response, identifier: string): Promise<void>;
}
