import Migration from "../db/Migration";
import {query} from "../db/MysqlConnectionManager";

/**
 * Must be the first migration
 */
export default class CreateMigrationsTable extends Migration {
    public async shouldRun(currentVersion: number): Promise<boolean> {
        try {
            await query('SELECT 1 FROM migrations LIMIT 1');
        } catch (e) {
            if (e.code !== 'ER_NO_SUCH_TABLE') {
                return false;
            }
        }

        return await super.shouldRun(currentVersion);
    }

    public async install(): Promise<void> {
        await this.query(`CREATE TABLE migrations
                          (
                              id             INT         NOT NULL,
                              name           VARCHAR(64) NOT NULL,
                              migration_date DATE,
                              PRIMARY KEY (id)
                          )`);
    }

    public async rollback(): Promise<void> {
        await this.query('DROP TABLE migrations');
    }
}
