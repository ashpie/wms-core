import Migration from "../db/Migration";

export default class DropLegacyLogsTable extends Migration {
    public async install(): Promise<void> {
        await this.query('DROP TABLE IF EXISTS logs');
    }

    public async rollback(): Promise<void> {
        // Do nothing
    }
}
