import Migration from "../db/Migration";

export default class DummyMigration extends Migration {
    public async install(): Promise<void> {
        // Do nothing
    }

    public async rollback(): Promise<void> {
        // Do nothing
    }
}
