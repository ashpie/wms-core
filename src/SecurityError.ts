export default class SecurityError implements Error {
    public readonly name: string = 'SecurityError';
    public readonly message: string;

    public constructor(message: string) {
        this.message = message;
    }
}
