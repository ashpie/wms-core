import {IncomingForm} from "formidable";
import Middleware from "./Middleware";
import {NextFunction, Request, Response} from "express";
import {FileError, ValidationBag} from "./db/Validator";

export default abstract class FileUploadMiddleware extends Middleware {
    protected abstract makeForm(): IncomingForm;

    protected abstract getDefaultField(): string;

    public async handle(req: Request, res: Response, next: NextFunction): Promise<void> {
        const form = this.makeForm();
        try {
            await new Promise<void>((resolve, reject) => {
                form.parse(req, (err, fields, files) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    req.body = fields;
                    req.files = files;
                    resolve();
                });
            });
        } catch (e) {
            const bag = new ValidationBag();
            const fileError = new FileError(e);
            fileError.thingName = this.getDefaultField();
            bag.addMessage(fileError);
            next(bag);
            return;
        }
        next();
    }
}
