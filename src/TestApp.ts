import Application from "../src/Application";
import Migration, {MigrationType} from "../src/db/Migration";
import ExpressAppComponent from "../src/components/ExpressAppComponent";
import RedisComponent from "../src/components/RedisComponent";
import MysqlComponent from "../src/components/MysqlComponent";
import NunjucksComponent from "../src/components/NunjucksComponent";
import LogRequestsComponent from "../src/components/LogRequestsComponent";
import MailComponent from "../src/components/MailComponent";
import SessionComponent from "../src/components/SessionComponent";
import AuthComponent from "../src/auth/AuthComponent";
import FormHelperComponent from "../src/components/FormHelperComponent";
import RedirectBackComponent from "../src/components/RedirectBackComponent";
import ServeStaticDirectoryComponent from "../src/components/ServeStaticDirectoryComponent";
import {Express} from "express";
import MagicLinkAuthMethod from "../src/auth/magic_link/MagicLinkAuthMethod";
import PasswordAuthMethod from "../src/auth/password/PasswordAuthMethod";
import {MAGIC_LINK_MAIL} from "./Mails";
import CreateMigrationsTable from "./migrations/CreateMigrationsTable";
import CreateUsersAndUserEmailsTableMigration from "./auth/migrations/CreateUsersAndUserEmailsTableMigration";
import CreateMagicLinksTableMigration from "./auth/magic_link/CreateMagicLinksTableMigration";
import AuthController from "./auth/AuthController";
import MagicLinkWebSocketListener from "./auth/magic_link/MagicLinkWebSocketListener";
import MagicLinkController from "./auth/magic_link/MagicLinkController";
import AddPasswordToUsersMigration from "./auth/password/AddPasswordToUsersMigration";
import AddNameToUsersMigration from "./auth/migrations/AddNameToUsersMigration";
import packageJson = require('../package.json');
import CsrfProtectionComponent from "./components/CsrfProtectionComponent";

export const MIGRATIONS = [
    CreateMigrationsTable,
    CreateUsersAndUserEmailsTableMigration,
    AddNameToUsersMigration,
    AddPasswordToUsersMigration,
    CreateMagicLinksTableMigration,
];

export default class TestApp extends Application {
    private readonly addr: string;
    private readonly port: number;
    private expressAppComponent?: ExpressAppComponent;
    private magicLinkWebSocketListener?: MagicLinkWebSocketListener<this>;

    public constructor(addr: string, port: number) {
        super(packageJson.version, true);
        this.addr = addr;
        this.port = port;
    }

    protected getMigrations(): MigrationType<Migration>[] {
        return MIGRATIONS;
    }

    protected async init(): Promise<void> {
        this.registerComponents();
        this.registerWebSocketListeners();
        this.registerControllers();
    }

    protected registerComponents(): void {
        this.expressAppComponent = new ExpressAppComponent(this.addr, this.port);
        const redisComponent = new RedisComponent();
        const mysqlComponent = new MysqlComponent();

        // Base
        this.use(this.expressAppComponent);
        this.use(new LogRequestsComponent());

        // Static files
        this.use(new ServeStaticDirectoryComponent('public'));

        // Dynamic views and routes
        this.use(new NunjucksComponent(['test/views', 'views']));
        this.use(new RedirectBackComponent());

        // Services
        this.use(mysqlComponent);
        this.use(new MailComponent());

        // Session
        this.use(redisComponent);
        this.use(new SessionComponent(redisComponent));

        // Utils
        this.use(new FormHelperComponent());

        // Middlewares
        this.use(new CsrfProtectionComponent());

        // Auth
        this.use(new AuthComponent(this, new MagicLinkAuthMethod(this, MAGIC_LINK_MAIL), new PasswordAuthMethod(this)));
    }

    protected registerWebSocketListeners(): void {
        this.magicLinkWebSocketListener = new MagicLinkWebSocketListener();
        this.use(this.magicLinkWebSocketListener);
    }

    protected registerControllers(): void {
        this.use(new AuthController());

        if (!this.magicLinkWebSocketListener) throw new Error('Magic link websocket listener not initialized.');
        this.use(new MagicLinkController(this.magicLinkWebSocketListener));
    }

    public getExpressApp(): Express {
        return this.as(ExpressAppComponent).getExpressApp();
    }
}
