import {v4 as uuid} from "uuid";
import {Logger as TsLogger} from "tslog";

export const log = new TsLogger();

export function makeUniqueLogger(): TsLogger {
    const id = uuid();
    return log.getChildLogger({
        requestId: id,
    });
}
