import WebSocket from "ws";
import {IncomingMessage} from "http";
import Application from "./Application";

export default abstract class WebSocketListener<T extends Application> {
    private app!: T;

    public init(app: T): void {
        this.app = app;
    }

    protected getApp(): T {
        return this.app;
    }

    public abstract path(): string;

    public abstract async handle(
        socket: WebSocket,
        request: IncomingMessage,
        session: Express.Session | null,
    ): Promise<void>;
}
