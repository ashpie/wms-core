import {Type} from "./Utils";

export default interface Extendable<ComponentClass> {
    as<C extends ComponentClass>(type: Type<C>): C;

    asOptional<C extends ComponentClass>(type: Type<C>): C | null;
}
