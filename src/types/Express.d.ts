import {Files} from "formidable";
import {Type} from "../Utils";
import Middleware from "../Middleware";
import {FlashMessages} from "../components/SessionComponent";
import {Logger} from "tslog";

declare global {
    namespace Express {
        export interface Request {
            log: Logger;

            getSession(): Session;


            files: Files;


            middlewares: Middleware[];

            as<M extends Middleware>(type: Type<M>): M;


            flash(): FlashMessages;

            flash(message: string): unknown[];

            flash(event: string, message: unknown): void;
        }

        export interface Response {
            redirectBack(defaultUrl?: string): void;
        }
    }
}
