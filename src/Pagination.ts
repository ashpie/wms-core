import Model from "./db/Model";

export default class Pagination<T extends Model> {
    private readonly models: T[];
    public readonly page: number;
    public readonly perPage: number;
    public readonly totalCount: number;

    public constructor(models: T[], page: number, perPage: number, totalCount: number) {
        this.models = models;
        this.page = page;
        this.perPage = perPage;
        this.totalCount = totalCount;
    }

    public hasPrevious(): boolean {
        return this.page > 1;
    }

    public hasNext(): boolean {
        return this.models.length >= this.perPage && this.page * this.perPage < this.totalCount;
    }

}
