import ApplicationComponent from "../ApplicationComponent";
import {Router} from "express";

export default class FormHelperComponent extends ApplicationComponent {
    public async init(router: Router): Promise<void> {
        router.use((req, res, next) => {
            let _validation: unknown | null;
            res.locals.validation = () => {
                if (!_validation) {
                    const v = req.flash('validation');
                    _validation = v.length > 0 ? v[0] : null;
                }

                return _validation;
            };

            let _previousFormData: unknown | null = null;
            res.locals.previousFormData = () => {
                if (!_previousFormData) {
                    const v = req.flash('previousFormData');
                    _previousFormData = v.length > 0 ? v [0] : null;
                }

                return _previousFormData;
            };
            next();
        });

        router.use((req, res, next) => {
            if (['GET', 'POST'].find(m => m === req.method)) {
                if (typeof req.body === 'object') {
                    req.flash('previousFormData', req.body);
                }
            }
            next();
        });
    }
}
