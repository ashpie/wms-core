import ApplicationComponent from "../ApplicationComponent";
import session from "express-session";
import config from "config";
import RedisComponent from "./RedisComponent";
import flash from "connect-flash";
import {Router} from "express";
import SecurityError from "../SecurityError";

export default class SessionComponent extends ApplicationComponent {
    private readonly storeComponent: RedisComponent;

    public constructor(storeComponent: RedisComponent) {
        super();
        this.storeComponent = storeComponent;
    }

    public async checkSecuritySettings(): Promise<void> {
        this.checkSecurityConfigField('session.secret');
        if (!config.get<boolean>('session.cookie.secure')) {
            throw new SecurityError('Cannot set cookie secure field to false.');
        }
    }

    public async init(router: Router): Promise<void> {
        router.use(session({
            saveUninitialized: true,
            secret: config.get('session.secret'),
            store: this.storeComponent.getStore(),
            resave: true,
            cookie: {
                httpOnly: true,
                secure: config.get('session.cookie.secure'),
                maxAge: config.get('session.cookie.maxAge'),
            },
            rolling: true,
        }));

        router.use(flash());

        router.use((req, res, next) => {
            req.getSession = () => {
                if (!req.session) throw new Error('Session not initialized.');
                return req.session;
            };

            res.locals.session = req.getSession();

            const _flash: FlashStorage = {};
            res.locals.flash = (key?: string): FlashMessages | unknown[] => {
                if (key !== undefined) {
                    if (_flash[key] === undefined) _flash[key] = req.flash(key);
                    return _flash[key] || [];
                }

                if (_flash._messages === undefined) {
                    _flash._messages = {
                        info: req.flash('info'),
                        success: req.flash('success'),
                        warning: req.flash('warning'),
                        error: req.flash('error'),
                    };
                }
                return _flash._messages;
            };
            next();
        });
    }
}

export type FlashMessages = {
    [k: string]: unknown[] | undefined
};

export type DefaultFlashMessages = FlashMessages & {
    info?: unknown[] | undefined;
    success?: unknown[] | undefined;
    warning?: unknown[] | undefined;
    error?: unknown[] | undefined;
};

type FlashStorage = FlashMessages & {
    _messages?: DefaultFlashMessages,
};
