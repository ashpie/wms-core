import ApplicationComponent from "../ApplicationComponent";
import express, {Express, Router} from "express";
import {log, makeUniqueLogger} from "../Logger";
import {Server} from "http";
import compression from "compression";
import Middleware from "../Middleware";
import {Type} from "../Utils";

export default class ExpressAppComponent extends ApplicationComponent {
    private readonly addr: string;
    private readonly port: number;
    private server?: Server;
    private expressApp?: Express;

    public constructor(addr: string, port: number) {
        super();
        this.addr = addr;
        this.port = port;
    }

    public async start(app: Express): Promise<void> {
        this.server = app.listen(this.port, this.addr, () => {
            log.info(`Web server running on ${this.addr}:${this.port}.`);
        });

        // Proxy
        app.set('trust proxy', 'loopback');

        this.expressApp = app;
    }

    public async init(router: Router): Promise<void> {
        router.use(express.json({
            type: req => req.headers['content-type']?.match(/^application\/(.+\+)?json$/),
        }));
        router.use(express.urlencoded({
            extended: true,
        }));

        // gzip
        router.use(compression());

        router.use((req, res, next) => {
            req.log = makeUniqueLogger();
            req.middlewares = [];
            req.as = <M extends Middleware>(type: Type<M>): M => {
                const middleware = req.middlewares.find(m => m.constructor === type);
                if (!middleware) throw new Error('Middleware ' + type.name + ' not present in this request.');
                return middleware as M;
            };
            next();
        });
    }

    public async stop(): Promise<void> {
        const server = this.server;
        if (server) {
            await this.close('Webserver', callback => server.close(callback));
        }
    }

    public getServer(): Server {
        if (!this.server) throw 'Server was not initialized.';
        return this.server;
    }

    public getExpressApp(): Express {
        if (!this.expressApp) throw new Error('Express app not initialized.');
        return this.expressApp;
    }
}
