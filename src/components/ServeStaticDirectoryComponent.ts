import ApplicationComponent from "../ApplicationComponent";
import express, {Router} from "express";
import {PathParams} from "express-serve-static-core";
import * as path from "path";

export default class ServeStaticDirectoryComponent extends ApplicationComponent {
    private readonly root: string;
    private readonly path?: PathParams;

    public constructor(root: string, routePath?: PathParams) {
        super();
        this.root = path.join(__dirname, '../../../', root);
        this.path = routePath;
    }

    public async init(router: Router): Promise<void> {
        if (this.path) {
            router.use(this.path, express.static(this.root, {maxAge: 1000 * 3600 * 72}));
        } else {
            router.use(express.static(this.root, {maxAge: 1000 * 3600 * 72}));
        }
    }

}
