import {Router} from "express";
import config from "config";
import * as child_process from "child_process";
import ApplicationComponent from "../ApplicationComponent";
import {ForbiddenHttpError} from "../HttpError";
import {log} from "../Logger";

export default class AutoUpdateComponent extends ApplicationComponent {
    private static async runCommand(command: string): Promise<void> {
        log.info(`> ${command}`);
        log.info(child_process.execSync(command).toString());
    }

    public async checkSecuritySettings(): Promise<void> {
        this.checkSecurityConfigField('gitlab_webhook_token');
    }

    public async init(router: Router): Promise<void> {
        router.post('/update/push.json', (req, res) => {
            const token = req.header('X-Gitlab-Token');
            if (!token || token !== config.get<string>('gitlab_webhook_token'))
                throw new ForbiddenHttpError('Invalid token', req.url);

            this.update(req.body).catch(req.log.error);

            res.json({
                'status': 'ok',
            });
        });
    }

    private async update(params: { [p: string]: unknown }) {
        log.info('Update params:', params);

        try {
            log.info('Starting auto update...');

            // Fetch
            await AutoUpdateComponent.runCommand(`git pull`);

            // Install new dependencies
            await AutoUpdateComponent.runCommand(`yarn install --production=false`);

            // Process assets
            await AutoUpdateComponent.runCommand(`yarn dist`);

            // Stop app
            await this.getApp().stop();

            log.info('Success!');
        } catch (e) {
            log.error(e, 'An error occurred while running the auto update.');
        }
    }
}
