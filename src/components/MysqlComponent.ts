import ApplicationComponent from "../ApplicationComponent";
import {Express} from "express";
import MysqlConnectionManager from "../db/MysqlConnectionManager";

export default class MysqlComponent extends ApplicationComponent {
    public async start(_app: Express): Promise<void> {
        await this.prepare('Mysql connection', () => MysqlConnectionManager.prepare());
    }

    public async stop(): Promise<void> {
        await MysqlConnectionManager.endPool();
    }

    public canServe(): boolean {
        return MysqlConnectionManager.isReady();
    }

}
