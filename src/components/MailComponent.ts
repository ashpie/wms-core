import ApplicationComponent from "../ApplicationComponent";
import {Express} from "express";
import Mail from "../Mail";
import config from "config";
import SecurityError from "../SecurityError";

export default class MailComponent extends ApplicationComponent {

    public async checkSecuritySettings(): Promise<void> {
        if (!config.get<boolean>('mail.secure')) {
            throw new SecurityError('Cannot set mail.secure (starttls) to false');
        }
        if (config.get<boolean>('mail.allow_invalid_tls')) {
            throw new SecurityError('Cannot set mail.allow_invalid_tls (ignore tls failure) to true');
        }
    }

    public async start(_app: Express): Promise<void> {
        await this.prepare('Mail connection', () => Mail.prepare());
    }

    public async stop(): Promise<void> {
        Mail.end();
    }

}
