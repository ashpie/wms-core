module.exports = {
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.test.json',
        }
    },
    transform: {
        "^.+\\.ts$": "ts-jest"
    },
    moduleFileExtensions: [
        'js',
        'ts',
        'd.ts'
    ],
    testMatch: [
        '**/test/**/*.test.ts'
    ],
    testEnvironment: 'node',
};